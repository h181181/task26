﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task24.Models;

namespace Task24.Controllers
{
    public class HomeController : Controller
    {

        public IActionResult Index()
        {
            SupervisorGroup.AddSupervisor(new Supervisor { Id = 1, Name = "Sam", IsAvailable = false }); ;
            SupervisorGroup.AddSupervisor(new Supervisor { Id = 2, Name="Carla"});
            SupervisorGroup.AddSupervisor(new Supervisor { Id = 3, Name="Sandra"});
            SupervisorGroup.AddSupervisor(new Supervisor { Id = 4, Name = "Anders" });
            SupervisorGroup.AddSupervisor(new Supervisor());
            SupervisorGroup.AddSupervisor(null);
           
            ViewBag.timeStamp = DateTime.Now;
            return View("MyFirstView");          
        }

        [HttpGet]
        public IActionResult SupervisorInfo()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SupervisorInfo(Supervisor supervisor)
        {
            if (ModelState.IsValid)
            {
                SupervisorGroup.Supervisors.Add(supervisor);

                return View("AddSupervisorConfirmation", supervisor);
            }
            else
            {
                return View("MyFirstView");
            }
        }


        [HttpGet]
        public IActionResult AllSupervisors(Supervisor supervisor)
        {
            List<string> SupervisorInfo = new List<string>();

            foreach (Supervisor sup in SupervisorGroup.Supervisors)
            {
                int? id = sup?.Id ?? 10;
                string name = sup?.Name ?? "Nameless";
                bool? isAvailable = sup?.IsAvailable ?? true;
                string subject = sup?.Subject ?? "Science";
                SupervisorInfo.Add($"Id: {id}, Name: {name}, Available: {isAvailable}, " +
                    $"Subject: {subject}");
            }
            return View(SupervisorInfo);
        }


        [HttpGet]
        public IActionResult SNameView(Supervisor supervisor)
        {
          var sList = SupervisorGroup.Supervisors.FindAll(supS => supS != null && (supS.Name[0].Equals('S') || supS.Name[0].Equals('s')));
          return View(sList);
        } 

    }
}
